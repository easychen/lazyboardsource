class Source
{
    weibo_fans_args = {
        "fields": [
            {
                "field_id":"weibo_fans_uid",
                "field_name":"微博UID",
                "field_desp_url":false,
                "field_default":"",
            },
            {
                "field_id":"weibo_fans_source_url",
                "field_name":"数据源URL·在LazyBoard中复制",
                "field_default":"",
            },
            {
                "field_id":"weibo_fans_check_minutes",
                "field_name":"抓取时间间隔·分钟数",
                "field_default":"0",
            },
            {
                "field_id":"weibo_fans_notes",
                "field_name":"笔记",
                "field_type":"textarea",
                "field_default":"",
            }
        ]
    }

    async weibo_fans_fetch( data )
    {
        const ret = await axios.get('https://m.weibo.cn/profile/info?uid='+data.weibo_fans_uid);
        if( ret?.data?.data )
            return ret.data.data.user.followers_count;
        else
            return "-";

    }

    customer_fetch_args = {
        "fields": [
            {
                "field_id":"customer_fetch_data_url",
                "field_name":"数据来源URL",
                "field_default":"",
            },
            {
                "field_id":"customer_fetch_code",
                "field_name":"执行代码·JavaScript",
                "field_type":"textarea",
                "field_default":"",
            },
            {
                "field_id":"customer_fetch_source_url",
                "field_name":"数据源URL·在LazyBoard中复制",
                "field_default":"",
            },
            {
                "field_id":"customer_fetch_check_minutes",
                "field_name":"抓取时间间隔·分钟数",
                "field_default":"0",
            }
        ]
    };

    async customer_fetch_fetch( data )
    {
        let ret_key = 'LB_CUSTOM_REST';
        eval("(async () => {" + data.customer_fetch_code + "})()");
        return window.localStorage.getItem(ret_key) || '-';

        // demo code 
        /**
        let result = await fetch( data.customer_fetch_data_url );
        const m1json = await result.json();
        window.localStorage.setItem( 'LB_CUSTOM_REST' , m1json.data.sct?.today_income_amount )
        **/
    }
}

window.source = new Source();
alert( 'loaded' );
